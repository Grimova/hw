package server;

import communication.ConfigMessage;
import communication.ConfigMessageType;
import communication.DataMessage;
import communication.Message;
import device.DeviceParameters;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


public class Server {
    private final int port;
    private final List<DeviceThread> loggedDevices;
    private final Map<String, Integer> deviceCountMap; // array containing number of received messages for each logged device
    private final Map<String, double[]> deviceValuesMap; // array containing last stored values for each logged device
    private boolean notInterrupted;


    public Server(int port) {
        this.port = port;
        loggedDevices = new ArrayList<>();
        deviceCountMap = new HashMap<>();
        deviceValuesMap = new HashMap<>();
    }

    public static void main(String[] args) {
        // The port number is hard fixed
        int portNumber = 8888;

        Server server = new Server(portNumber);
        server.start();
    }

    public void start() {
        notInterrupted = true;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.printf("Waiting for devices on port %s.\n", port);

            while (notInterrupted) {
                // Accepting of new sockets
                Socket socket = serverSocket.accept();

                // Start a different thread for each device
                DeviceThread deviceThread = new DeviceThread(socket);
                deviceThread.start();
            }
            // Log out all created devices
            for (DeviceThread deviceThread : loggedDevices) {
                deviceThread.inputStream.close();
                deviceThread.outputStream.close();
                deviceThread.socket.close();
                deviceThread.close();
            }
            serverSocket.close();
            deviceValuesMap.clear();
            deviceCountMap.clear();

        } catch (IOException e) {
            System.out.println("Exception in server socket.");
        }
    }

    class DeviceThread extends Thread {
        private final Socket socket;
        private ObjectInputStream inputStream;
        private ObjectOutputStream outputStream;
        private String deviceName; // name of the device
        private Message message; // received message
        private boolean keepRunning = true;

        DeviceThread(Socket socket) {
            this.socket = socket;
            try {
                outputStream = new ObjectOutputStream(socket.getOutputStream());

                inputStream = new ObjectInputStream(socket.getInputStream());
                ConfigMessage configMessage = (ConfigMessage) inputStream.readObject();
                // Log a new device
                if (ConfigMessageType.LOGIN.equals(configMessage.getConfigMessageType())) {
                    deviceName = configMessage.getDeviceName();
                    loggedDevices.add(this);
                    deviceCountMap.put(deviceName, 0);
                    deviceValuesMap.put(deviceName, null);
                    System.out.printf("Device %s was connected.", deviceName);
                }
            } catch (IOException e) {
                System.out.println("IO exception when streams were created.");
            } catch (ClassNotFoundException e) {
                System.out.println("Unknown type of the message.");
            }
        }

        public void run() {
            while (keepRunning) {
                try {
                    message = (Message) inputStream.readObject(); // read a general message
                } catch (IOException e) {
                    System.out.printf("It was impossible to read a message from device %s.\n", deviceName);
                    break;
                } catch (ClassNotFoundException e) {
                    System.out.println("Unknown type of the message.");
                }

                // Processing of configuration messages
                if (message instanceof ConfigMessage) {
                    if (ConfigMessageType.LOGOUT.equals(((ConfigMessage) message).getConfigMessageType())) {
                        System.out.println("All connections will be disconnected.");
                        for (DeviceThread deviceThread : loggedDevices) {
                            deviceThread.keepRunning = false;
                        }
                        notInterrupted = false;
                    } else if (ConfigMessageType.LOGIN.equals(((ConfigMessage) message).getConfigMessageType())) {
                        continue;
                    } else if (ConfigMessageType.SEND_COUNT.equals(((ConfigMessage) message).getConfigMessageType())) {
                        if (((ConfigMessage) message).getOtherDeviceName() == null) {
                            // return count of received messages for each device
                            for (String name : deviceCountMap.keySet()) {
                                if (!name.equals(DeviceParameters.CONTROLLING_DEVICE)) {
                                    System.out.printf("The amount of received messages for device %s is %d.\n", name, deviceCountMap.get(name));
                                }
                            }
                        } else {
                            // return count of received messages only for this device
                            String otherDeviceName = ((ConfigMessage) message).getOtherDeviceName();
                            if (deviceCountMap.containsKey(otherDeviceName)) {
                                System.out.printf("The amount of received messages for device %s is %d.\n", otherDeviceName, deviceCountMap.get(otherDeviceName));
                            }
                        }
                    } else if (ConfigMessageType.SEND_DATA.equals(((ConfigMessage) message).getConfigMessageType())) {
                        if (((ConfigMessage) message).getOtherDeviceName() == null) {
                            // return data of received messages for each device
                            for (String name : deviceValuesMap.keySet()) {
                                if (!name.equals(DeviceParameters.CONTROLLING_DEVICE)) {
                                    System.out.printf("The values from device %s are %s\n", name, Arrays.toString(deviceValuesMap.get(name)));
                                }
                            }
                        } else {
                            // return data of received messages only for this device
                            String otherDeviceName = ((ConfigMessage) message).getOtherDeviceName();
                            if (deviceValuesMap.containsKey(otherDeviceName)) {
                                System.out.printf("The values from device %s are %s\n", otherDeviceName, Arrays.toString(deviceValuesMap.get(otherDeviceName)));
                            }
                        }
                    } else {
                        System.out.println("Not recognized controlling command.");
                    }

                } else if (message instanceof DataMessage) {
                    // process of data message
                    if (deviceCountMap.containsKey(message.getDeviceName())) {
                        // Increase the number of received messages
                        deviceCountMap.computeIfPresent(message.getDeviceName(), (k, v) -> v + 1);
                        // Replace old values by the new values
                        deviceValuesMap.put(deviceName, ((DataMessage) message).getValues());
                    }
                } else {
                    throw new NotImplementedException();
                }
            }
            close();
        }

        private void close() {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                System.out.println("IO exception during streams and socket closing.");
            }
        }
    }
}
