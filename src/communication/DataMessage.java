package communication;

public class DataMessage extends Message {
    private double[] values;

    public DataMessage(String deviceName, double[] values) {
        super(deviceName);
        this.values = values;
    }

    public double[] getValues() {
        return values;
    }
}
