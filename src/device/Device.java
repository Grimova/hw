package device;

import communication.ConfigMessage;
import communication.ConfigMessageType;
import communication.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class Device {
    // A general device object
    protected final String deviceName;
    private final int port;
    private final String server;
    protected ObjectInputStream inputStream;
    protected ObjectOutputStream outputStream;
    protected boolean notInterrupted = true;
    private Socket socket;


    Device(String server, int port, String deviceName) {
        this.server = server;
        this.port = port;
        this.deviceName = deviceName;
    }

    public static void main(String[] args) {
        DeviceParameters deviceParameters = new DeviceParameters();
        deviceParameters.parse(args);

        int port = 8888;
        String serverAddress = "localhost";
        String deviceName = deviceParameters.getDeviceName();

        Device device;
        // If a device is a controlling one is given by the input parameters
        if (deviceParameters.isControlling()) {
            System.out.println("New controlling device is to be initialized.");
            device = ControllingDevice.getInstance(serverAddress, port, deviceName);
        } else {
            System.out.printf("New data device %s is to be initialized.", deviceName);
            device = new DataDevice(serverAddress, port, deviceName);
        }

        if (!device.start()) {
            return;
        }

        device.run();
    }

    protected abstract void run();

    public boolean start() {
        try {
            socket = new Socket(server, port);

            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.out.printf("Exception in device socket for device %s", deviceName);
            return false;
        }
        login();
        return true;
    }

    private void login() {
        sendMessage(new ConfigMessage(deviceName, ConfigMessageType.LOGIN, null));
    }

    void sendMessage(Message msg) {
        try {
            outputStream.writeObject(msg);
        } catch (IOException e) {
            notInterrupted = false;
        }
    }

    protected void close() {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            System.out.println("IO exception during streams and socket closing.");
        }
    }
}
