package device;

import communication.ConfigMessage;
import communication.ConfigMessageType;
import java.util.Scanner;

public class ControllingDevice extends Device {
    private static ControllingDevice instance;
    private final Scanner scanner;

    private ControllingDevice(String server, int port, String deviceName) {
        super(server, port, deviceName);
        scanner = new Scanner(System.in);
    }

    public static ControllingDevice getInstance(String server, int port, String deviceName) {
        // The controlling device can be only one at the time
        if (instance == null) {
            instance = new ControllingDevice(server, port, deviceName);
        }
        return instance;
    }


    @Override
    protected void run() {
        // Reading inputs from the command line
        System.out.println("Waiting for inputs:");

        while (notInterrupted) {
            String line = scanner.nextLine();
            String[] splits = line.split(" ");
            String command = splits[0];
            String toDevice = splits.length == 1 ? null : splits[1];
            if (ConfigMessageType.SEND_DATA.getValue().equals(command)) {
                sendMessage(new ConfigMessage(deviceName, ConfigMessageType.SEND_DATA, toDevice));
            } else if (ConfigMessageType.SEND_COUNT.getValue().equals(command)) {
                sendMessage(new ConfigMessage(deviceName, ConfigMessageType.SEND_COUNT, toDevice));
            } else if (ConfigMessageType.LOGOUT.getValue().equals(command)) {
                sendMessage(new ConfigMessage(deviceName, ConfigMessageType.LOGOUT, toDevice));
                break;
            } else {
                System.out.printf("Unrecognized command %s.\n", command);
            }
        }
        scanner.close();
        instance.close();
    }
}
