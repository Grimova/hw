package communication;

public class ConfigMessage extends Message{
    private ConfigMessageType configMessageType;
    private String otherDeviceName;

    public ConfigMessage(String deviceName, ConfigMessageType configMessageType, String otherDeviceName) {
        super(deviceName);
        this.configMessageType = configMessageType;
        this.otherDeviceName = otherDeviceName;
    }

    public ConfigMessageType getConfigMessageType() {
        return configMessageType;
    }

    public String getOtherDeviceName() {
        return otherDeviceName;
    }
}
