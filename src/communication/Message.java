package communication;

import java.io.Serializable;

public class Message implements Serializable {
    private String deviceName;

    public Message(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
