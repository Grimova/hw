package device;

import communication.DataMessage;
import java.util.Random;

public class DataDevice extends Device {
    public static final int MAX = 100;

    public DataDevice(String server, int port, String deviceName) {
        super(server, port, deviceName);
    }

    protected void run() {
        try {
            while (notInterrupted) {
                // Generate new values and send them to the server
                sendMessage(new DataMessage(deviceName, generateOutput()));
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.printf("Run of the data device %s was interrupted.\n", deviceName);
        }
    }

    private double[] generateOutput() {
        return new Random().doubles().limit(MAX).toArray();
    }
}