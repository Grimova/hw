package device;

public class DeviceParameters {
    public static final String CONTROLLING_DEVICE = "controlling";
    private String deviceName;

    public void parse(String[] args) {
        if (args.length == 0) {
            System.out.println("New controlling device");
            deviceName = CONTROLLING_DEVICE;
        } else {
            deviceName = args[0];
        }
    }

    public String getDeviceName() {
        return deviceName;
    }

    public boolean isControlling() {
        return deviceName.equals(CONTROLLING_DEVICE);
    }
}
