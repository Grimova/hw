package communication;

public enum ConfigMessageType {
    SEND_DATA("data"),
    SEND_COUNT("count"),
    LOGIN("login"),
    LOGOUT("logout");

    private final String value;

    ConfigMessageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
