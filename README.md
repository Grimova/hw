# Homework

The application is based on the basic server/client paradigma. Devices communicate with the server and server process their requests and sent data. 

The structure of the project is the following:

- **communication** - contains classes of messages which are sent between the server and devices
- **device** - contains classes of used devices
- **server** - contains the server class

The project is already built, the JAR files can be found in the folder *\out\artifacts\homework_eaton_jar* and *\out\artifacts\homework_eaton_jar2*/
The server.jar file can be simply run by using the command:

 ```java -jar server.jar```

 It is possible to launch the file device.jar by simply running the command:

  ```java -jar device.jar <name>```

where <name> is the name of the new device which will be generating data and sending them to the server.

When the device.jar file is run without parameters, the new "controlling" device will be launched. This kind of device is used for the communication with the server.

There is the list of commands which can be sent through controlling device to the server:

- **count** <name> - the server will print the number of acquired messages for the device with the name <name>. If no name is specified, then the number of messages for all devices is printed.
- **data** <name> - the server will print values obtained from device <name>. If no name is specified, values of all devices are printed. Note that only the last amount of data is printed - it is done this way in order to prevent using too much memory. 
- **logout** - all devices will be disconnected.

Let me denote that I used only the standards output in the application.
